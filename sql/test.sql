USE test;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(20) DEFAULT NULL COMMENT '姓名',
  `sex` ENUM('男','女','妖') DEFAULT NULL COMMENT '性别',
  `age` INT(3) DEFAULT NULL COMMENT '年龄',
  `phone` VARCHAR(64) DEFAULT NULL COMMENT '手机',
  `address` VARCHAR(128) DEFAULT NULL COMMENT '地址',
  `createTime` DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updateTime` DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

INSERT INTO users(`name`,`sex`,`age`,`phone`,`address`) VALUES
('申小慧','男',18,'13007520668','20-2713'),
('张默涵','男',18,'13043734877','20栋11层10号'),
('王利娜','男',18,'13083719278','12号楼1707'),
('杨萍','男',18,'13137129018','8号楼23楼91号'),
('宋莹莹','男',18,'13137703258','8号楼2单元5楼东'),
('宋照阳','男',18,'13137738167','20号楼806'),
('笑','男',18,'13162135524','呃呃'),
('张','男',18,'13203728680','13楼东14'),
('娄小朋','男',18,'13203802677','9号楼7楼40户'),
('李靖','男',18,'13213202148','21层57I号'),
('李萍','男',18,'13223059731','19号楼2903'),
('刘一凡','男',18,'13223084822','22号楼2单元3楼东户'),
('王琳','男',18,'13253528887','3号楼21层东二户'),
('秋风万里','男',18,'13255965001','13号楼'),
('申琳','男',18,'13271578383','十号楼三单元6楼东'),
('陈冠宇','男',18,'13298162488','十一号楼五单元一楼西户'),
('丁亮','男',18,'13333755322','13号楼29楼113'),
('周雅慧','男',18,'13333818060','中亨本色20楼东5'),
('杨曜铭','男',18,'13383782221','8楼西13'),
('Jason','男',18,'13500000000','4567'),
('张晓燕','男',18,'13503819740','中亨本色16楼东10'),
('陈麦','男',18,'13523002939','15-105'),
('杨磊磊','男',18,'13523521579','中亨本色1616'),
('康春凤','男',18,'13525511620','、'),
('璐璐','男',18,'13525569788','13号楼3单元'),
('刘玉焕','男',18,'13525575381','22号楼3单元2楼西户'),
('张香华','男',18,'13526408137','东区商都路十里铺20号楼1单元5楼西户'),
('吴莉娜','男',18,'13526426992','20号楼2710'),
('李倩颖','男',18,'13526659526','中亨本色六楼东123'),
('王昆','男',18,'13526701838','一号楼17楼66号'),
('冯鹏丹','男',18,'13526754555','10号楼10楼38号'),
('丁悦','男',18,'13592410671','16号楼3单元2楼东户'),
('胡智涵','男',18,'13592508592','沙口路1号汉飞金沙国际20＃'),
('王雪','男',18,'13592687879','中亨本色6楼121'),
('王玺','男',18,'13598867980','13号楼4单元4楼东户'),
('张','男',18,'13598896369','17-30-119'),
('王子','男',18,'13598899025','19号楼1505'),
('我','男',18,'13600333321','22栋505'),
('张可','男',18,'13603998330','26号楼21层575室'),
('牛琳','男',18,'13608670711','16号群楼324'),
('暴蕾','男',18,'13613712821','20号楼四单元二楼西户'),
('程媛媛','男',18,'13613811493','14号楼16层西二'),
('宋振宇','男',18,'13633822272','17号楼2单元18层东1户（209室）'),
('心心','男',18,'13633825692','汉飞金沙4-13-52'),
('刘雪珂','男',18,'13633826758','9号楼14层79最东户'),
('李鑫','男',18,'13640548901','3号楼'),
('蔡攀','男',18,'13653800611','20楼534号'),
('刘军','男',18,'13653810591','4号楼1单元1楼西'),
('陈震','男',18,'13663004708','中亨本色楼六楼东十六'),
('屈峰涛','男',18,'13663719352','20号楼304'),
('张桂月','男',18,'13663810727','3号楼10层东2'),
('郝莉','男',18,'13663820236','26号楼19层533'),
('沉美','男',18,'13673606470','16号楼2单元3楼东'),
('何丽','男',18,'13673699979','17号楼二单元13-东一'),
('阴云龙','男',18,'13676993133','20楼'),
('杨金丽','男',18,'13700866211','1号楼18楼71号'),
('周玲','男',18,'13703921715','中享本色'),
('西小薯','男',18,'13703925193','19号楼204'),
('张国轩','男',18,'13703990647','18楼西九'),
('秦涵','男',18,'13733810019','汉飞金沙国际17号楼1单元4楼 西一'),
('许婷','男',18,'13782785412','5-17-66'),
('范敏','男',18,'13783629939','13楼东14户'),
('孙珏','男',18,'13803863051','20号楼11楼1111'),
('胡勇星','男',18,'13814452225','888'),
('张梦园','男',18,'13837116400','20号楼702室'),
('董凯','男',18,'13837121492','五号楼2单元'),
('李佳','男',18,'13837152020','中亨幼儿园'),
('马子涵','男',18,'13838126664','十一号楼四单元三楼西户'),
('孙小兔','男',18,'13838165277','11号楼1楼4号'),
('董元伟','男',18,'13838223872','七号楼三单元七楼东'),
('陈媛媛','男',18,'13838304272','18楼西5'),
('徐磊','男',18,'13838580053','8号楼7层西2'),
('萧晓','男',18,'13849199238','17号楼1单元15楼东2'),
('赵萍','男',18,'13903825977','5楼西13'),
('余静','男',18,'13938256397','6号楼6号'),
('曹钰琳','男',18,'13939063559','13栋24层96号'),
('马浩','男',18,'13949020007','10号楼10楼东'),
('yyh','男',18,'13999999999','gghhhj'),
('Nathan','男',18,'15002120625','45号楼1802室'),
('张晨静','男',18,'15003803302','中亨本色8楼197号'),
('艾','男',18,'15037166027','19号楼，3305'),
('丁元元','男',18,'15038105855','16楼434'),
('张海燕','男',18,'15038394224','中亨本色16楼东10'),
('耿晓静','男',18,'15039086822','14号楼19层75号'),
('杨飞','男',18,'15083197008','20号楼'),
('刘明','男',18,'15090521315','16层489'),
('王金龙','男',18,'15090663556','中亨本色大楼 19楼，西1  533室'),
('何云浩','男',18,'15093232071','20号楼1单元4楼东'),
('董高祥','男',18,'15136225800','20楼218'),
('顾丹丹','男',18,'15138696929','12号楼1704'),
('季瑞','男',18,'15188328980','中亨幼儿园'),
('徐沛','男',18,'15236609916','4号楼1单元11号'),
('张淑莉','男',18,'15238001702','17号楼1单元1楼2号'),
('张倩倩','男',18,'15238630091','5号楼'),
('韩家乐','男',18,'15286808996','16   324'),
('桑迪','男',18,'15286828388','18号楼1558室'),
('乔金玉','男',18,'15290879776','汉飞金沙国际18号24层96'),
('赵登辉','男',18,'15290882168','12楼312'),
('丁慧娇','男',18,'15294640399','中亨本色2楼东4'),
('王家宝','男',18,'15303829365','9号楼17层100号'),
('刘国芳','男',18,'15324963362','15号楼501'),
('来女士','男',18,'15333807518','汉飞十号楼22层87'),
('何小漩','男',18,'15378723997','9号楼1902'),
('高秋林','男',18,'15378731566','13号楼24层西2'),
('缪梦颖洁','男',18,'15515511109','11号楼6层37号'),
('韩丰锐','男',18,'15515625935','26-17-463'),
('马双霞','男',18,'15515808063','18号楼4单元'),
('张鹤一','男',18,'15517115012','七号楼五单元四楼西'),
('李珊珊','男',18,'15522756533','20号楼913'),
('胡璇念','男',18,'15537180502','10号楼15楼58号'),
('董岩','男',18,'15538113533','20号楼218室'),
('刘海超','男',18,'15538210580','15楼393'),
('一','男',18,'15563952874','在于'),
('郑好好','男',18,'15602130747','七号楼25层东2户'),
('阿飞','男',18,'15617801285','5号楼，3楼西一户，12号'),
('李燕宁','男',18,'15638225160','14号楼8层东一'),
('曹俊','男',18,'15639257535','11'),
('管泽启qi','男',18,'15824866505','11号楼2层14户'),
('李需要','男',18,'15824886886','一号楼'),
('支大伟','男',18,'15824891841','中亨本色13楼西12'),
('李婉鑫','男',18,'15838010788','5号楼2单元2楼东户'),
('王仁杰','男',18,'15838118500','17-1-25-99'),
('张彪','男',18,'15838188320','9号楼8层46号'),
('李晶','男',18,'15838189675','中亨本色12楼316'),
('薛官鸿','男',18,'15838191224','18号楼'),
('郑嘉晨','男',18,'15838284729','牛咯拒绝'),
('马甲奇','男',18,'15838399621','11号楼6单元4楼东户'),
('不能','男',18,'15863251425','y'),
('刘','男',18,'15890060860','19层533室'),
('李洋','男',18,'15890089439','4号楼1单元2楼西'),
('焦晓燕','男',18,'15890095624','25b2单元5楼东'),
('王娜','男',18,'15890195735','11-26-177'),
('张静','男',18,'15890656901','15号楼3单元7楼东户'),
('钱超颖','男',18,'15890674565','13号楼28号'),
('孙静普','男',18,'15896622172','郑州市汉飞金沙国际小区17号楼2单元18层209室'),
('任磊','男',18,'15936203853','20号楼1305'),
('郭彬','男',18,'15937159035','10号楼'),
('苗慧','男',18,'15937198241','中亨本色16楼416室'),
('李兵辉','男',18,'15938721849','16号楼3单元5楼东'),
('杨敏','男',18,'15938721894','16号楼三单元5楼东'),
('毛毛','男',18,'15939897686','13号楼'),
('郭淑云','男',18,'15981836305','9号楼2单元一楼东户'),
('马苗苗','男',18,'15994193736','17楼464'),
('韩洋洋','男',18,'17603953233','15楼'),
('丁利冰','男',18,'17737503721','十号楼三单元六楼东'),
('冯岩','男',18,'18003811138','15楼-58'),
('朱亚军','男',18,'18037500530','12楼，东15户'),
('赵红亮','男',18,'18037689529','18/6/21'),
('王刚','男',18,'18039553989','11号楼17层114'),
('测试','男',18,'18117502913','益江路'),
('王五','男',18,'18117502915','河南街'),
('张三','男',18,'18117502917','益江路'),
('估计','男',18,'18117502999','哦哦哦'),
('我的小','男',18,'18125454548','3013楼403室'),
('冯松萍','男',18,'18137170519','6号楼32层128号'),
('周猛','男',18,'18137378371','17号1单元32西一'),
('程勇','男',18,'18202485432','11号楼三单元一楼西'),
('刘心霓','男',18,'18203683605','金水区沙口路汉飞金沙国际小区12号楼2705'),
('王心蕙','男',18,'18236958018','6号楼9层35号'),
('张艳艳','男',18,'18239999005','7号楼1单元3楼东'),
('张孟迪','男',18,'18339997010','十七楼470'),
('仁慈','男',18,'18503820809','中亨本色6楼东12'),
('李龙','男',18,'18530001091','17号楼2单元6楼F'),
('卢蒙蒙','男',18,'18530027066','13-3-7'),
('周颖','男',18,'18530066055','7号楼25-东二'),
('张艳','男',18,'18530086585','中亨幼儿园'),
('王宇','男',18,'18530218919','8号楼12层东二'),
('吴','男',18,'18530835299','10号'),
('岳建刚','男',18,'18530936875','14号楼13层50号'),
('乐丹','男',18,'18530966168','锦隆阳光小区18栋3单元502室'),
('海飞哥','男',18,'18530974388','汉飞金沙17-1-04'),
('高照恒','男',18,'18537113211','19楼533室'),
('张英','男',18,'18538258266','中亨本色四楼东十一户'),
('李艳','男',18,'18538502503','14栋2单元602'),
('赵小璐','男',18,'18538787117','15号楼1805室'),
('陈露','男',18,'18539285151','12号楼2单元四楼西'),
('李颍涛','男',18,'18539299922','中亨本色11楼271'),
('海慧','男',18,'18539936667','21楼 东1'),
('庞娟','男',18,'18613717792','22号楼三单元五楼西'),
('李娟','男',18,'18613731567','19栋102'),
('王琨','男',18,'18622113678','17号楼1单元32层127室'),
('刘志明','男',18,'18625530661','中亨本色20楼563户'),
('欧阳莉娟','男',18,'18625565220','中亨本色18楼477'),
('詹一帆','男',18,'18625588644','3号楼3楼11号'),
('刘映雪','男',18,'18637500026','15号楼2102'),
('王嘉怡','男',18,'18638206577','20号楼713'),
('张淼淼','男',18,'18638229907','8号楼10层东2'),
('杨慧敏','男',18,'18638267692','19号楼403'),
('张伯伦','男',18,'18638577731','14层   西5'),
('景晓颖','男',18,'18638660565','10号楼10楼38号'),
('王芳','男',18,'18638799716','15号楼3103'),
('牛尚娅','男',18,'18638928259','汉飞国际11号9层63'),
('王萌萌','男',18,'18695892226','14号楼79号'),
('王艳辉','男',18,'18703688135','五楼西'),
('王欢','男',18,'18703970386','16号楼904户'),
('冉冉','男',18,'18736000233','20号楼517'),
('王丽','男',18,'18736017631','20号楼 1517'),
('艾秀秀','男',18,'18738121950','17号楼1单元622'),
('张君傲','男',18,'18738183315','13号29层113'),
('赵漫飞','男',18,'18739598615','菡美美容院'),
('李思佳','男',18,'18768889324','20号楼九楼913'),
('陈靖瀛','男',18,'18818802119','农业路天明路怡丰新都汇'),
('朱风华','男',18,'18838085058','郑州市金水区南阳路中亨本色13号348'),
('袁月春','男',18,'18843692666','14号楼23楼91'),
('顾娜','男',18,'18903717934','10号楼'),
('翟海伦','男',18,'18937123505','20号楼1104'),
('大宝','男',18,'18937136210','17 104'),
('吴臻','男',18,'18937172636','17-1-1-04'),
('胡春霞','男',18,'18937670279','9楼230'),
('王子','男',18,'18939507173','19号楼102'),
('吴','男',18,'18965232569','让人');