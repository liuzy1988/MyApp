# SpringMVC + Freemarker + jQueryEJS + MyBatis项目模板

- 演示地址[https://liuzy88.com/MyApp/](https://liuzy88.com/MyApp/)

# 做了什么?

- SpringMVC框架，响应html（静态页面）、ftl（Freemarker渲染后的DOM）、json（可在前端用jQueryEJS渲染成DOM）
- `MyServletContextListener`应用初始化，实现外部配置文件
- `MyFilter`前置过滤器，实现设置编码、验证
- SpringTask定时任务，项目常用
- MyBatis操作MySQL
- MyBatis插件打印格式化的带参数的SQL语句

# jQuery的load方法

- 基于jQuery的load，组合、刷新页面数据

```js
$('#dataContainer').load('template_url' + '?key=value', function() {
	// callback();
});
```