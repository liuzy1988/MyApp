package com.myapp.test;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.StringTokenizer;

import com.myapp.util.FormatJson;
import com.myapp.util.FormatSql;

/**
 * 
 * @author liuzy
 * @since 2016年9月1日
 */
public class Test {
	public static void main(String[] args) {
		// testFormatJson();
		testFormatSql();
	}

	public static void testFormatJson() {
		String json = "{\"updateApps\":[],\"forcedApps\":[{\"aid\":357,\"id\":\"com.example.ceshi_1\",\"name\":\"CeShi_1\",\"rate\":0,\"favorCount\":0,\"downloadCount\":11,\"info\":\"77\",\"packageUrl\":\"https://172.16.26.7/client/rc/dl/12345678900000212345612/4/c1472626542520236.apk\",\"icon\":\"https://172.16.26.7/files/AppstoreRes/image/1472627248752919.png\",\"size\":802006,\"version\":\"1.0\",\"versionInfo\":\"\",\"developer\":\"瞿峰\",\"upTime\":\"2016-08-31 14:57:32\",\"forcedStatus\":\"1\",\"forcedInstall\":\"1\",\"previewImg\":[\"https://172.16.26.7/files/AppstoreRes/image/1472627268575764.png\",\"https://172.16.26.7/files/AppstoreRes/image/1472627278658705.png\",\"https://172.16.26.7/files/AppstoreRes/image/1472627273564967.png\"],\"recommend\":null,\"category\":\"银联应用\",\"gVersion\":null,\"orderno\":null},{\"aid\":359,\"id\":\"com.example.me\",\"name\":\"Me\",\"rate\":4,\"favorCount\":1632,\"downloadCount\":13,\"info\":\"me\",\"packageUrl\":\"https://172.16.26.7/client/rc/dl/12345678900000212345612/4/c1472627565390103.apk\",\"icon\":\"https://172.16.26.7/files/AppstoreRes/image/1472627565377576.png\",\"size\":802014,\"version\":\"1.0\",\"versionInfo\":\"\",\"developer\":\"瞿峰\",\"upTime\":\"2016-08-31 15:14:41\",\"forcedStatus\":\"1\",\"forcedInstall\":\"1\",\"previewImg\":[\"https://172.16.26.7/files/AppstoreRes/image/1472627614943101.png\",\"https://172.16.26.7/files/AppstoreRes/image/1472627625130414.png\",\"https://172.16.26.7/files/AppstoreRes/image/1472627620545918.png\"],\"recommend\":null,\"category\":\"银联应用\",\"gVersion\":null,\"orderno\":null},{\"aid\":360,\"id\":\"com.example.tangtang\",\"name\":\"tangtang\",\"rate\":1,\"favorCount\":3,\"downloadCount\":3,\"info\":\"tang\",\"packageUrl\":\"https://172.16.26.7/client/rc/dl/12345678900000212345612/4/c1472627763923265.apk\",\"icon\":\"https://172.16.26.7/files/AppstoreRes/image/1472627763912862.png\",\"size\":801260,\"version\":\"1.0\",\"versionInfo\":\"\",\"developer\":\"瞿峰\",\"upTime\":\"2016-08-31 15:17:12\",\"forcedStatus\":\"1\",\"forcedInstall\":\"1\",\"previewImg\":[\"https://172.16.26.7/files/AppstoreRes/image/1472627790564112.png\",\"https://172.16.26.7/files/AppstoreRes/image/1472627802616279.png\",\"https://172.16.26.7/files/AppstoreRes/image/1472627797110429.png\"],\"recommend\":null,\"category\":\"银联应用\",\"gVersion\":null,\"orderno\":null},{\"aid\":344,\"id\":\"com.baidu.BaiduMap\",\"name\":\"百度地图\",\"rate\":0,\"favorCount\":0,\"downloadCount\":79,\"info\":\"地图\",\"packageUrl\":\"https://172.16.26.7/client/rc/dl/12345678900000212345612/4/c1471419737436566.apk\",\"icon\":\"https://172.16.26.7/files/AppstoreRes/image/1471497207572397.png\",\"size\":38985390,\"version\":\"9.4.0\",\"versionInfo\":\"\",\"developer\":\"瞿峰\",\"upTime\":\"2016-08-17 16:15:23\",\"forcedStatus\":\"1\",\"forcedInstall\":\"1\",\"previewImg\":[\"https://172.16.26.7/files/AppstoreRes/image/1471417767918497.png\",\"https://172.16.26.7/files/AppstoreRes/image/1471417762483857.png\",\"https://172.16.26.7/files/AppstoreRes/image/1471417756743146.png\"],\"recommend\":null,\"category\":\"其他应用\",\"gVersion\":null,\"orderno\":null}],\"instructions\":[{\"id\":\"3\",\"type\":\"1\",\"appId\":\"com.qihoo.freewifi\",\"packageUrl\":\"https://172.16.26.7/client/rc/dl/12345678900000212345612/6/1471503019187754.apk\",\"version\":\"3.9.0\"},{\"id\":\"6\",\"type\":\"1\",\"appId\":\"com.youku.phone\",\"packageUrl\":\"https://172.16.26.7/client/rc/dl/12345678900000212345612/6/1471854681367341.apk\",\"version\":\"5.8.1\"},{\"id\":\"7\",\"type\":\"1\",\"appId\":\"com.centerm.upgrade.solibs\",\"packageUrl\":\"https://172.16.26.7/client/rc/dl/12345678900000212345612/6/1471854771300460.apk\",\"version\":\"1.2.0\"}]}";
		System.out.println(FormatJson.format(json));
	}

	public static void testFormatSql() {
		String sql = "insert into order_info (orderId,goodsId,goodsInfoId, goodsName,goodsIcon, price, spec, specInfo, buyCount ) values (?,?,?, ?,?, ?, ?, ?, ? )";
		System.out.println(FormatSql.format(sql));
		sql = "insert into `orders` ( `orderId`, userId, `type`, teamInfoId, robId, robSerialNumber, `status`, oldAmount, couponSN, amount, dpId, dpName, dpTell, dpAddress, duName, duPhoneNumber, duVoucher) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		System.out.println(FormatSql.format(sql));
		sql = "select id, userName, phoneNumber, openId, nickName, avatar, sex, language, city, province, country, dpId, lastLocation, createTime, updateTime from `users` where openId = ?";
		System.out.println(FormatSql.format(sql));
		sql = "SELECT DATE_FORMAT(b.t_create, '%Y-%c-%d') dateID, b.title memo     FROM (SELECT id FROM `orc_scheme_detail` d WHERE d.business=208     AND d.type IN (29,30,31,321,33,34,3542,361,327,38,39,40,41,42,431,4422,415,4546,47,48,'a',    29,30,31,321,33,34,3542,361,327,38,39,40,41,42,431,4422,415,4546,47,48,'a')     AND d.title IS NOT NULL AND t_create >=     DATE_FORMAT((DATE_SUB(NOW(),INTERVAL 1 DAY)),'%Y-%c-%d') AND t_create     < DATE_FORMAT(NOW(), '%Y-%c-%d') ORDER BY d.id LIMIT 2,10) a,     orc_scheme_detail b WHERE a.id = b.id";
		System.out.println(new SQLFormatter().format(sql));
	}
}

class SQLFormatter {

	private static final Set<String> BEGIN_CLAUSES = new HashSet<String>();
	private static final Set<String> END_CLAUSES = new HashSet<String>();
	private static final Set<String> LOGICAL = new HashSet<String>();
	private static final Set<String> QUANTIFIERS = new HashSet<String>();
	private static final Set<String> DML = new HashSet<String>();
	private static final Set<String> MISC = new HashSet<String>();
	public static final String WHITESPACE = " \n\r\f\t";

	static {
		BEGIN_CLAUSES.add("left");
		BEGIN_CLAUSES.add("right");
		BEGIN_CLAUSES.add("inner");
		BEGIN_CLAUSES.add("outer");
		BEGIN_CLAUSES.add("group");
		BEGIN_CLAUSES.add("order");

		END_CLAUSES.add("where");
		END_CLAUSES.add("set");
		END_CLAUSES.add("having");
		END_CLAUSES.add("join");
		END_CLAUSES.add("from");
		END_CLAUSES.add("by");
		END_CLAUSES.add("join");
		END_CLAUSES.add("into");
		END_CLAUSES.add("union");

		LOGICAL.add("and");
		LOGICAL.add("or");
		LOGICAL.add("when");
		LOGICAL.add("else");
		LOGICAL.add("end");

		QUANTIFIERS.add("in");
		QUANTIFIERS.add("all");
		QUANTIFIERS.add("exists");
		QUANTIFIERS.add("some");
		QUANTIFIERS.add("any");

		DML.add("insert");
		DML.add("update");
		DML.add("delete");

		MISC.add("select");
		MISC.add("on");
	}

	static final String indentString = "    ";
	static final String initial = "\n    ";

	public String format(String source) {
		return new FormatProcess(source).perform();
	}

	private static class FormatProcess {
		boolean beginLine = true;
		boolean afterBeginBeforeEnd = false;
		boolean afterByOrSetOrFromOrSelect = false;
		boolean afterValues = false;
		boolean afterOn = false;
		boolean afterBetween = false;
		boolean afterInsert = false;
		int inFunction = 0;
		int parensSinceSelect = 0;
		private LinkedList<Integer> parenCounts = new LinkedList<Integer>();
		private LinkedList<Boolean> afterByOrFromOrSelects = new LinkedList<Boolean>();

		int indent = 0;

		StringBuilder result = new StringBuilder();
		StringTokenizer tokens;
		String lastToken;
		String token;
		String lcToken;

		public FormatProcess(String sql) {
			tokens = new StringTokenizer(sql, "()+*/-=<>'`\"[]," + WHITESPACE, true);
		}

		public String perform() {

			result.append("\n");

			while (tokens.hasMoreTokens()) {
				token = tokens.nextToken();
				lcToken = token.toLowerCase();

				if ("'".equals(token)) {
					String t;
					do {
						t = tokens.nextToken();
						token += t;
					} while (!"'".equals(t) && tokens.hasMoreTokens()); // cannot
																		// handle
																		// single
																		// quotes
				} else if ("\"".equals(token)) {
					String t;
					do {
						t = tokens.nextToken();
						token += t;
					} while (!"\"".equals(t));
				}

				if (afterByOrSetOrFromOrSelect && ",".equals(token)) {
					commaAfterByOrFromOrSelect();
				} else if (afterOn && ",".equals(token)) {
					commaAfterOn();
				}

				else if ("(".equals(token)) {
					openParen();
				} else if (")".equals(token)) {
					closeParen();
				}

				else if (BEGIN_CLAUSES.contains(lcToken)) {
					beginNewClause();
				}

				else if (END_CLAUSES.contains(lcToken)) {
					endNewClause();
				}

				else if ("select".equals(lcToken)) {
					select();
				}

				else if (DML.contains(lcToken)) {
					updateOrInsertOrDelete();
				}

				else if ("values".equals(lcToken)) {
					values();
				}

				else if ("on".equals(lcToken)) {
					on();
				}

				else if (afterBetween && lcToken.equals("and")) {
					misc();
					afterBetween = false;
				}

				else if (LOGICAL.contains(lcToken)) {
					logical();
				}

				else if (isWhitespace(token)) {
					white();
				}

				else {
					misc();
				}

				if (!isWhitespace(token)) {
					lastToken = lcToken;
				}

			}
			return result.toString();
		}

		private void commaAfterOn() {
			out();
			indent--;
			newline();
			afterOn = false;
			afterByOrSetOrFromOrSelect = true;
		}

		private void commaAfterByOrFromOrSelect() {
			out();
			newline();
		}

		private void logical() {
			if ("end".equals(lcToken)) {
				indent--;
			}
			newline();
			out();
			beginLine = false;
		}

		private void on() {
			indent++;
			afterOn = true;
			newline();
			out();
			beginLine = false;
		}

		private void misc() {
			out();
			if ("between".equals(lcToken)) {
				afterBetween = true;
			}
			if (afterInsert) {
				newline();
				afterInsert = false;
			} else {
				beginLine = false;
				if ("case".equals(lcToken)) {
					indent++;
				}
			}
		}

		private void white() {
			if (!beginLine) {
				result.append(" ");
			}
		}

		private void updateOrInsertOrDelete() {
			out();
			indent++;
			beginLine = false;
			if ("update".equals(lcToken)) {
				newline();
			}
			if ("insert".equals(lcToken)) {
				afterInsert = true;
			}
		}

		@SuppressWarnings({ "UnnecessaryBoxing" })
		private void select() {
			out();
			indent++;
			newline();
			parenCounts.addLast(Integer.valueOf(parensSinceSelect));
			afterByOrFromOrSelects.addLast(Boolean.valueOf(afterByOrSetOrFromOrSelect));
			parensSinceSelect = 0;
			afterByOrSetOrFromOrSelect = true;
		}

		private void out() {
			result.append(token);
		}

		private void endNewClause() {
			if (!afterBeginBeforeEnd) {
				indent--;
				if (afterOn) {
					indent--;
					afterOn = false;
				}
				newline();
			}
			out();
			if (!"union".equals(lcToken)) {
				indent++;
			}
			newline();
			afterBeginBeforeEnd = false;
			afterByOrSetOrFromOrSelect = "by".equals(lcToken) || "set".equals(lcToken) || "from".equals(lcToken);
		}

		private void beginNewClause() {
			if (!afterBeginBeforeEnd) {
				if (afterOn) {
					indent--;
					afterOn = false;
				}
				indent--;
				newline();
			}
			out();
			beginLine = false;
			afterBeginBeforeEnd = true;
		}

		private void values() {
			indent--;
			newline();
			out();
			indent++;
			newline();
			afterValues = true;
		}

		@SuppressWarnings({ "UnnecessaryUnboxing" })
		private void closeParen() {
			parensSinceSelect--;
			if (parensSinceSelect < 0) {
				indent--;
				parensSinceSelect = parenCounts.removeLast().intValue();
				afterByOrSetOrFromOrSelect = afterByOrFromOrSelects.removeLast().booleanValue();
			}
			if (inFunction > 0) {
				inFunction--;
				out();
			} else {
				if (!afterByOrSetOrFromOrSelect) {
					indent--;
					newline();
				}
				out();
			}
			beginLine = false;
		}

		private void openParen() {
			if (isFunctionName(lastToken) || inFunction > 0) {
				inFunction++;
			}
			beginLine = false;
			if (inFunction > 0) {
				out();
			} else {
				out();
				if (!afterByOrSetOrFromOrSelect) {
					indent++;
					newline();
					beginLine = true;
				}
			}
			parensSinceSelect++;
		}

		private static boolean isFunctionName(String tok) {
			final char begin = tok.charAt(0);
			final boolean isIdentifier = Character.isJavaIdentifierStart(begin) || '"' == begin;
			return isIdentifier && !LOGICAL.contains(tok) && !END_CLAUSES.contains(tok) && !QUANTIFIERS.contains(tok) && !DML.contains(tok) && !MISC.contains(tok);
		}

		private static boolean isWhitespace(String token) {
			return WHITESPACE.indexOf(token) >= 0;
		}

		private void newline() {
			result.append("\n");
			for (int i = 0; i < indent; i++) {
				result.append(indentString);
			}
			beginLine = true;
		}
	}

}
