package com.myapp.task;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 * 
 * @author liuzy
 * @since 2016年8月30日
 */
@Component
public class MyTask {
	private Logger logger = Logger.getLogger(MyTask.class);

	public void run() {
		logger.info("MyTask run()");
	}
}
