package com.myapp.mapper;

import java.util.List;
import java.util.Map;

import com.myapp.entity.UserEntity;

public interface UserEntityMapper {
	Integer findUserCount(Map<String, Object> map);

	List<UserEntity> findUserList(Map<String, Object> map);
	
	List<UserEntity> testShowSql(Map<String, Object> map);
}
