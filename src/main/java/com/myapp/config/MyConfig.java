package com.myapp.config;

import java.util.Properties;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

/**
 * 
 * @author liuzy
 * @since 2016年8月30日
 */
public class MyConfig extends PropertyPlaceholderConfigurer {
	private static Properties props;

	@Override
	protected void processProperties(ConfigurableListableBeanFactory beanFactoryToProcess, Properties props) throws BeansException {
		super.processProperties(beanFactoryToProcess, props);
		MyConfig.props = props;
	}

	public static String get(String key) {
		return props.getProperty(key);
	}

	public static String get(String key, String defaultValue) {
		String value = props.getProperty(key);
		return value != null && !value.isEmpty() ? value : defaultValue;
	}
}
