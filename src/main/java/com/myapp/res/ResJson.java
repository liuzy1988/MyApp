package com.myapp.res;

import java.util.List;

/**
 * 
 * @author liuzy
 * @since 2016年8月30日
 */
public class ResJson extends Res {
	private String resCode = "200";
	private String resMsg = "成功";

	public ResJson(int count, List<?> list) {
		super(count, list);
	}

	public String getResCode() {
		return resCode;
	}

	public void setResCode(String resCode) {
		this.resCode = resCode;
	}

	public String getResMsg() {
		return resMsg;
	}

	public void setResMsg(String resMsg) {
		this.resMsg = resMsg;
	}
}
