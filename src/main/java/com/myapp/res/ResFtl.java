package com.myapp.res;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author liuzy
 * @since 2016年8月30日
 */
public class ResFtl extends Res {
	public static final int PAGE_WIDTH = 7;
	List<Integer> pages = new ArrayList<Integer>();

	public ResFtl(int count, List<?> list) {
		super(count, list);
		int start = 1;
		int end = PAGE_WIDTH;
		if (totalPage > PAGE_WIDTH) {
			if (page > PAGE_WIDTH / 2) {// 大于一半时往右移动
				int move = page - PAGE_WIDTH / 2;
				start += move;
				end += move;
				if (end > totalPage) {// 移动后超过了
					int out = end - totalPage;
					end = totalPage;
					start -= out;
				}
			}
		} else {
			end = totalPage;
		}
		for (int i = start; i <= end; i++) {
			pages.add(i);
		}
	}

	public List<Integer> getPages() {
		return pages;
	}

	public void setPages(List<Integer> pages) {
		this.pages = pages;
	}
}
