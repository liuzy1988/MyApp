package com.myapp.res;

import java.util.List;

import com.myapp.req.Req;

/**
 * 
 * @author liuzy
 * @since 2016年8月30日
 */
public class Res {
	protected int page;
	protected int rows;
	protected int totalPage;
	protected int totalCount;
	protected List<?> datas;

	public Res(int count, List<?> list) {
		this.page = Req.page();
		this.rows = Req.rows();
		this.totalCount = count;
		this.datas = list;
		this.totalPage = totalCount <= rows ? 1 : (totalCount % rows == 0 ? totalCount / rows : totalCount / rows + 1);
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public List<?> getDatas() {
		return datas;
	}

	public void setDatas(List<?> datas) {
		this.datas = datas;
	}
}
