package com.myapp.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 
 * @author liuzy
 * @since 2016年8月30日
 */
@Controller
@RequestMapping("/")
public class IndexController extends BaseController {
	private Logger logger = Logger.getLogger(IndexController.class);

	@RequestMapping("/")
	public String index() {
		logger.info("IndexController index()");
		return "index.html";
	}
}
