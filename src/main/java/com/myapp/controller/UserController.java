package com.myapp.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.myapp.res.ResFtl;
import com.myapp.res.ResJson;
import com.myapp.service.UserService;

/**
 * 
 * @author liuzy
 * @since 2016年8月30日
 */
@Controller
@RequestMapping("/user")
public class UserController extends BaseController {
	private Logger logger = Logger.getLogger(UserController.class);

	@Autowired
	private UserService userService;

	@RequestMapping("/ftl")
	public String ftl(String keywords, ModelMap model) {
		logger.info("UserController ftl()");
		int count = userService.findUserCount(keywords);
		List<?> list = userService.findUserList(keywords);
		model.addAttribute("keywords", keywords);
		model.addAttribute("res", new ResFtl(count, list));
		return "ftl/users.ftl";
	}

	@RequestMapping("/json")
	@ResponseBody
	public ResJson json(String keywords) {
		logger.info("UserController json()");
		int count = userService.findUserCount(keywords);
		List<?> list = userService.findUserList(keywords);
		return new ResJson(count, list);
	}
	
	@RequestMapping("/testShowSql")
	@ResponseBody
	public Object testShowSql() {
		return userService.testShowSql();
	}
}
