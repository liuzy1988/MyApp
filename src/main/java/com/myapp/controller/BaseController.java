package com.myapp.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 
 * @author liuzy
 * @since 2016年8月30日
 */
@Component
public class BaseController {
	@Autowired
	public HttpServletRequest request;
}
