package com.myapp.req;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * 
 * @author liuzy
 * @since 2016年8月30日
 */
public class Req {
	public static final String PAGE = "page";
	public static final String ROWS = "rows";
	public static final int DEFALUT_ROWS = 15;

	public static HttpServletRequest getRequest() {
		return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
	}

	public static int page() {
		try {
			return Integer.parseInt(getRequest().getParameter(PAGE));
		} catch (Exception e) {
			return 1;
		}
	}

	public static int rows() {
		try {
			return Integer.parseInt(getRequest().getParameter(ROWS));
		} catch (Exception e) {
			return DEFALUT_ROWS;
		}
	}

	public static void pagination(Map<String, Object> map) {
		int page = page();
		int rows = rows();
		map.put("offset", (page - 1) * rows);
		map.put("limit", rows);
	}
}
