package com.myapp.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myapp.entity.UserEntity;
import com.myapp.mapper.UserEntityMapper;
import com.myapp.req.Req;

/**
 * 
 * @author liuzy
 * @since 2016年8月30日
 */
@Service
public class UserService {
	private Logger logger = Logger.getLogger(UserService.class);

	@Autowired
	private UserEntityMapper userEntityMapper;

	public Integer findUserCount(String keywords) {
		logger.info("UserService findUserCount()");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("keywords", keywords);
		return userEntityMapper.findUserCount(map);
	}

	public List<UserEntity> findUserList(String keywords) {
		logger.info("UserService userList()");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("keywords", keywords);
		Req.pagination(map);
		return userEntityMapper.findUserList(map);
	}
	
	public List<UserEntity> testShowSql() {
		logger.info("UserService testShowSql()");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("keywords", "王");
		List<String> phoneList = new ArrayList<String>();
		phoneList.add("18638799716");
		phoneList.add("13137129018");
		phoneList.add("18039553989");
		phoneList.add("18703970386");
		map.put("phoneList", phoneList);
		try {
			map.put("start", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2016-08-01 11:11:11"));
		} catch (ParseException e) {
			map.put("start", null);
			e.printStackTrace();
		}
		map.put("end", "2016-09-1 23:59:59");
		Req.pagination(map);
		return userEntityMapper.testShowSql(map);
	}
}
