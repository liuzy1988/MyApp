// 页面对象封装
function Pager(dom, ftl, pms) {
	var pager = {
		dom : dom,
		ftl : ftl,
		query : {},
		requery : function() {},
		callback : function() {}
	};
	if (pms) for (k in pms) {
		pager[k] = pms[k];
	}
	return pager;
}
// 页面加载封装
function load(pager) {
	var dom = pager.dom;
	var ftl = pager.ftl;
	var query = pager.query;
	var loaded = pager.loaded;
	var pms = [];
	for (k in query) {
		if (k == 'keywords')
			pms.push(k + '=' + encodeURIComponent(query[k]));
		else
			pms.push(k + '=' + query[k]);
	}
	if (pms.length > 0) {
		ftl += '?' + pms.join('&');
		ftl = ftl.replace(/\ /g, '%20');
	}
	$(dom).load(ftl, function() {
		// 分页
		var pagination = $(dom + ' .pagination li');
		if (pagination) {
			pagination.click(function() {
				pager.query.page = $(this).data('page');
				load(pager);
			});
		}
		// 搜索
		var searchTxt = $(dom + ' .search input');
		var searchBtn = $(dom + ' .search button');
		if (searchTxt && searchBtn) {
			searchBtn.click(function() {
				pager.query.keywords = searchTxt.val();
				load(pager);
			});
			searchTxt.focus(function() {
				$(this).keydown(function(e) {
					if (e.which == 13) {
						pager.query.keywords = searchTxt.val();
						load(pager);
					}
				});
			});
		}
		loaded && loaded();
	});
}