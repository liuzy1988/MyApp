<ul class="pagination">
	<#-- 总数 -->
    <li class="total">共${res.totalCount}条 共${res.totalPage}页</li>
	<#-- 首页和上一页 -->
    <#if res.page==1>
    	<li class="frist disable">首页</li>
	    <li class="prve disable">上一页</li>
    <#else>
    	<li class="frist" data-page="1">首页</li>
	    <li class="prve" data-page="${res.page - 1}">上一页</li>
    </#if>
	<#-- 翻页 -->
	<#list res.pages as page>
		<#if page==res.page>
			<li class="page on" data-page="${page}">${page}</li>
		<#else>
			<li class="page" data-page="${page}">${page}</li>
		</#if>
    </#list>
	<#-- 下一页和尾页 -->
    <#if res.page==res.totalPage>
    	<li class="next disable">下一页</li>
	    <li class="last disable">尾页</li>
    <#else>
	    <li class="next" data-page="${res.page + 1}">下一页</li>
	    <li class="last" data-page="${res.totalPage}">尾页</li>
    </#if>
</ul>
