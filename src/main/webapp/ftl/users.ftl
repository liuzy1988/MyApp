<div>
	<div class="search">
		<input type="text" placeholder="请输入关键字" value="${keywords!""}">
		<button></button>
	</div>
</div>
<table class="table">
	<thead>
		<tr>
			<th>姓 名</th>
			<th>手 机</th>
			<th>地 址</th>
		</tr>
	</thead>
	<tbody>
		<#list res.datas as user>
			<tr>
				<td>${user.name!""}</td>
				<td>${user.phone!""}</td>
				<td>${user.address!""}</td>
			</tr>
		</#list>
	</tbody>
</table>
<#include "pagination.ftl" />
